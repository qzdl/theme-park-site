# 4.4.2 - Part 3
Read the instruction for part 3 of the team project, organise your teamwork and submit the completed material for peer review. We recommend to make a submission within one week. When you are ready, return to week 8 and make a submission in 'Peer-graded Assignment: 4.4.3 Team assignment. Part 3'.

In this assignment, your group will apply appearance CSS properties to the HTML for the theme park web site that you designed in the first part of the assignment. You will prepare a CSS stylesheet and upload it onto the Workspace: static web pages that is located in week 10 so that it can style your web pages.

## The assignment has the following parts:

### Markup wireframes with Appearance CSS

Review the three wireframes that you produced for the large screen version of your web site from part 2 of the team project and add to them to ensure that they have the appearance that you wish your site to have. Ensure that you have agreed the colours that will be applied to the elements. Agree the font family or specific font that you will use for different parts of your site, and the colours that should be used.

Work through the wireframes and decide on the correct CSS to build those pages, and add classes and identities to the pages. Mark up your wireframes accordingly. Upload a .zip file of these new marked-up versions of the wireframes into the Workspace: static web pages in week 10.

### Set up and link stylesheet

Code the CSS stylesheet with these appearance styling instructions and link them to the correct HTML elements using the class and id mechanism.

### Upload the pages to the static web page system

Upload the .zip file of your three new versions of HTML pages to the Workspace: static web page system in week 10 with the class and id properties and the CSS stylesheet within a tidy directory structure. Test that everything is working as expected by loading the pages in a browser using the shareable URL for your web site.

### Validate the code in the pages

Validate your code using the [W3C code validator](https://validator.w3.org/).
